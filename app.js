const number = /(?:0|[1-9][\d]*)(?:\.[\d]+)?/
const getXY = new RegExp(`\\((${number.source})\\,(${number.source})\\)`)
const validator = new RegExp(`^(?:${getXY.source})(?: ${getXY.source})*$`)

const transform = (string) => {
  if (validator.test(string)) {
    let array = string.split(/ /)
    let table = []
    array.map((element) => {
      let tmp = element.match(getXY)
      if (tmp && tmp.length === 3) {
        table.push([Number(tmp[1]), Number(tmp[2])])
      }
    })
    return table
  } else return [0, 0]
}

const getCoord = (pointList, index) => {
  if (pointList.length > 0) {
    return pointList.map((point) => point[index])
  } else {
    return []
  }
}

const zigma = (list) => {
  let zigma = 0
  if (list.length > 0) {
    list.map((element) => zigma += element)
  }
  return zigma
}

const pow2 = (list) => {
  if (list.length > 0) {
    return list.map((element) => element * element)
  } else return 0
}

const product = (listA, listB) => {
  if (listA.length > 0 && listB.length > 0 && listA.length === listB.length) {
    return listA.map((element, index) => element * listB[index])
  } else return 0
}

const media = (i, n) => i / n

const S = (a, a2, n) => a2 - ((a * a) / n)

const Sab = (xy, x, y, n) => xy - ((x * y) / n)

const Beta1 = (xy, xx) => xy / xx

const Beta0 = (x, y, b) => y - (b * x)

const R = (xy, xx, yy) => (xy / (Math.sqrt(xx * yy)))

const getData = (string) => {
  let xy = transform(string)
  let n = xy.length
  let Xi = getCoord(xy, 0)
  let zigmaXi = zigma(Xi)
  let Xi2 = pow2(Xi)
  let zigmaXi2 = zigma(Xi2)
  let Yi = getCoord(xy, 1)
  let zigmaYi = zigma(Yi)
  let Yi2 = pow2(Yi)
  let zigmaYi2 = zigma(Yi2)
  let XiYi = product(Xi, Yi)
  let zigmaXiYi = zigma(XiYi)
  let _x = media(zigmaXi, n)
  let _y = media(zigmaYi, n)
  let Sxx = S(zigmaXi, zigmaXi2, n)
  let Syy = S(zigmaYi, zigmaYi2, n)
  let Sxy = Sab(zigmaXiYi, zigmaXi, zigmaYi, n)
  let B1 = Beta1(Sxy, Sxx)
  let B0 = Beta0(_x, _y, B1)
  let r = R(Sxy, Sxx, Syy)
  let r2 = r * r
  return {
    xy,
    n,
    Xi,
    Yi,
    Xi2,
    Yi2,
    XiYi,
    _x,
    _y,
    zigma: {
      Xi: zigmaXi,
      Yi: zigmaYi,
      Xi2: zigmaXi2,
      Yi2: zigmaYi2,
      XiYi: zigmaXiYi
    },
    S: {
      xx: Sxx,
      yy: Syy,
      xy: Sxy
    },
    Beta: {
      b1: B1,
      b0: B0
    },
    r,
    r2
  }
}

// console.log(getData('(1,1) (2,2) (3,2) (4,3) (5,5) (6,5)'))

let entrada = document.getElementById('entrada')
let data = {}
let entradaOnChange = (event, i, v) => {
  let value = v || i || event.target.value
  let valid = validator.test(value)
  entrada.className = (valid) ? 'valid' : 'invalid'
  if (valid) {
    updateData(value)
  }
  console.log(data)
  entrada.value = value
}

const printXY = (data) => {
  let table = document.getElementById('xy-table-content')
  let content = ''
  data.map((point) => {
    content += `<tr><td>${point[0]}</td><td>${point[1]}</td></tr>`
  })
  table.innerHTML = content
}

const printResult = (data) => {
  let table = document.getElementById('data-table-content')
  let footer = document.getElementById('data-table-zigma')
  let content = ''
  for (let i = 0; i < data.n; i++) {
    content += `<tr>
    <td>${data.Xi[i]}</td>
    <td>${data.Yi[i]}</td>
    <td>${data.Xi2[i]}</td>
    <td>${data.Yi2[i]}</td>
    <td>${data.XiYi[i]}</td>
    </tr>`
  }
  table.innerHTML = content
  footer.innerHTML = `<tr>
  <td>Ʃ : <b>${data.zigma.Xi}</b></td>
  <td>Ʃ : <b>${data.zigma.Yi}</b></td>
  <td>Ʃ : <b>${data.zigma.Xi2}</b></td>
  <td>Ʃ : <b>${data.zigma.Yi2}</b></td>
  <td>Ʃ : <b>${data.zigma.XiYi}</b></td>
  </tr>`
}

const printFooter = (data) => {
  let footer = document.getElementById('data-print')
  footer.innerHTML = `
  <h2> Resultados </h2>
    <span><b>Ʃ Xi: </b> ${data.zigma.Xi}</span><br/>
    <span><b>Ʃ Xi^2: </b> ${data.zigma.Xi2}</span><br/>
    <span><b>Ʃ Yi: </b> ${data.zigma.Yi}</span><br/>
    <span><b>Ʃ Yi^2: </b> ${data.zigma.Yi2}</span><br/>
    <span><b>Ʃ XiYi: </b> ${data.zigma.XiYi}</span><br/>
    <span><b>_X: </b> ${data._x}</span><br/>
    <span><b>_Y: </b> ${data._y}</span><br/>
    <span><b>Sxx: </b> ${data.S.xx}</span><br/>
    <span><b>Syy: </b> ${data.S.yy}</span><br/>
    <span><b>Sxy: </b> ${data.S.xy}</span><br/>
    <span><b>β1: </b> ${data.Beta.b1}</span><br/>
    <span><b>β0: </b> ${data.Beta.b0}</span><br/>
    <span><b>r: </b> ${data.r}</span><br/>
    <span><b>r^2: </b> ${data.r2}</span><br/>
  `
}

let updateData = (value) => {
  data = getData(value)
  printXY(data.xy)
  printResult(data)
  printFooter(data)
  google.charts.load('current', {'packages':['corechart']})

  function drawChart () {
    let content = google.visualization.arrayToDataTable([
      ['X', 'Y'], ...data.xy])

    let options = {
      title: 'Grafica de Regresión lineal simple',
      hAxis: {title: 'X'},
      vAxis: {title: 'Y'},
      legend: 'none',
      trendlines: { 0: {} }    // Draw a trendline for data series 0.
    }

    let chart = new google.visualization.ScatterChart(document.getElementById('chart'))
    chart.draw(content, options)
  }
  google.charts.setOnLoadCallback(drawChart)
}

entrada.addEventListener('input', entradaOnChange, false)
